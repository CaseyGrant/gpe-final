﻿using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Transform target; // gets what the camera looks at

    public float smoothSpeed = 0.125f; // sets the speed that the camera smooths
    public Vector3 offset; // sets the distance from the target

    void FixedUpdate()
    {
        Vector3 desiredPosition = target.position + offset; // sets the position of the camera
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed); // sets the position while smoothing
        transform.position = smoothedPosition; // sets the camera position

        transform.LookAt(target); // looks at the target
    }
}
