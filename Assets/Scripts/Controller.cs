﻿using UnityEngine;

public class Controller : MonoBehaviour
{
    public Animator anim; // sets the animator
    public Rigidbody rb; // sets the rigidbody
    public Transform tf; // sets the transform

    public bool movementRun; // a bool to determine if running
    public float speed; // sets the speed

    public Vector3 moveDirection = Vector3.zero; // sets the move direction

    public void Rotate(Transform tf, ref bool movementWalk, int rotationSpeed)
    {
        if (Input.GetAxis("Horizontal") != 0) // if D, A, LeftArrow, or RightArrow is pressed
        {
            if (Input.GetAxis("Horizontal") > 0)
            {
                tf.Rotate(0, rotationSpeed * Time.deltaTime, 0); // rotates
            }
            else
            {
                tf.Rotate(0, -rotationSpeed * Time.deltaTime, 0); // rotates
            }
            movementWalk = true; // sets walk to true
        }
    }

    public void jump(Rigidbody rb, float jumpForce)
    {
        rb.AddForce(transform.up * jumpForce); // lifts the pawn up
    }

    public void Forward(Rigidbody rb, Transform tf, float speed, ref bool movementRun)
    {
        rb.AddForce((transform.forward * speed)); // moves the pawn forward
        movementRun = true; // sets run to true
    }
}
