﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null; // allows the game manager to be accessed

    public int Score; // hold the score

    void Awake() // ensures that there is always a game manager and that there is only ever one
    {
        if (instance == null) // if instance is empty
        {
            instance = this; //make this the instance
        }
        else if (instance != this) // if instance is not this 
        {
            Destroy(gameObject); // destroy this
        }

        DontDestroyOnLoad(gameObject); // keeps the game manger through scenes
    }

    void Update()
    {
        if (Input.GetKeyDown("escape")) // if you press escape
        {
            SceneManager.LoadScene("Menu"); // loads the menu
        }
    }
}
