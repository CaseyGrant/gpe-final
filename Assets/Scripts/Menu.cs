﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public void Feast()
    {
        GameManager.instance.Score = 0; // resets the score
        SceneManager.LoadScene("Feast"); // changes scene
    }

    public void Endless()
    {
        GameManager.instance.Score = 0; // resets the score
        SceneManager.LoadScene("Platformer"); // changes scene
    }

    public void Quit()
    {
        Application.Quit(); // quits the game
    }
}
