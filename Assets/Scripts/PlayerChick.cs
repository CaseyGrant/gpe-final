﻿using UnityEngine;

public class PlayerChick : Controller
{
    public float jumpforce; // sets the jump force

    private bool jumping; // sets if jumping

    void Start()
    {
        anim = GetComponent<Animator>(); // gets the animator
        rb = GetComponent<Rigidbody>(); // gets the rigidbody
        tf = GetComponent<Transform>(); // gets the transform
    }

    void Update()
    {
        anim.SetBool("Run", movementRun); // updates the animation
        anim.SetBool("Jump", jumping); // updates the animation

        moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical")); // sets the move direction
        moveDirection *= speed; // times by speed

        if (Input.GetAxis("Horizontal") != 0)
        {
            if (Input.GetAxis("Horizontal") > 0)
            {
                if (Input.GetKey("left shift"))
                {
                    Forward(rb, tf, speed * 1.2f, ref movementRun); // moves forward sprint
                }
                else
                {
                    Forward(rb, tf, speed, ref movementRun); // moves forward
                }

                tf.rotation = new Quaternion(0, 0, 0, 0); // sets rotation to 0
            }

            if (Input.GetAxis("Horizontal") < 0)
            {
                if (Input.GetKey("left shift"))
                {
                    Forward(rb, tf, speed * 1.2f, ref movementRun); // moves forward sprint
                }
                else
                {
                    Forward(rb, tf, speed, ref movementRun); // moves forward
                }

                tf.rotation = new Quaternion(0, 180, 0, 0); // sets rotation to 180
            }
        }

        if ((Input.GetKeyDown("w") || Input.GetKeyDown("space")) && !jumping)
        {
            jump(rb, jumpforce); // jumps
            jumping = true; // sets jumping to true
        }

        if (Input.GetAxis("Horizontal") == 0)
        {
            movementRun = false; // sets run to false
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ground"))
        {
            jumping = false; // sets jumping to false
        }
        if (other.CompareTag("Death"))
        {
            gameObject.SetActive(false); // disables player
        }
    }
}
