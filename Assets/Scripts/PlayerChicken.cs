﻿using System.Collections;
using UnityEngine;

public class PlayerChicken : Controller
{
    public int rotationSpeed; // sets rotation speed
    public GameObject grass; // sets the grass prefab

    private bool movementWalk; // sets walking
    private bool move; // sets moving
    private bool eat; // sets eating
    private bool eaten; // sets eaten
    private GameObject grassInstance; // sets grass instance

    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>(); // gets the animator
        rb = GetComponent<Rigidbody>(); // gets the rigidbody
        tf = GetComponent<Transform>(); // gets the transform
        grassInstance = grass; // sets grass to grass
    }

    void Update()
    {
        anim.SetBool("Walk", movementWalk); // updates the animation
        anim.SetBool("Run", movementRun); // updates the animation
        anim.SetBool("Eat", eat); // updates the animation

        moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical")); // sets move direction
        moveDirection *= speed; // Times by speed
        if (move == false)
        {
            if (Input.GetAxis("Horizontal") != 0)
            {
                Rotate(tf, ref movementWalk, rotationSpeed); // rotates
            }

            if (Input.GetAxis("Vertical") != 0 && movementWalk == false)
            {
                if (Input.GetAxis("Vertical") > 0)
                {
                    rb.velocity = tf.forward * speed; // moves forward
                    movementRun = true; // set run to true
                }
            }
        }
        if (Input.GetKey("e"))
        {
            eat = true; // sets eat to true
            move = true; // sets move to true
        }
        else
        {
            eat = false; // sets eat to false
            move = false; // sets move to false
        }
        if (Input.GetAxis("Vertical") == 0)
        {
            movementRun = false; // sets run to true
        }
        if (Input.GetAxis("Horizontal") == 0)
        {
            movementWalk = false; // sets walk to true

        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Grass"))
        {
            if (eat)
            {
                Destroy(other.gameObject, 1.5f); // destroy grass
                Destroy(other); // destroy grass's collider
                StartCoroutine(Consume()); // starts a co-routine to wait
            }
        }
    }
    IEnumerator Consume()
    {
        yield return new WaitForSeconds(1.5f); // waits 1.5 seconds
        speed += 1; // increases the speed
        tf.localScale += new Vector3 (1, 1, 1); // increases scale
        GameManager.instance.Score++; // increases score
        var position = new Vector3(Random.Range(-30, 30), -0.0511f, Random.Range(-30, 30)); // gets a random position
        grassInstance = Instantiate(grass, position, Quaternion.identity); // spawns in grass
        grassInstance.transform.localScale += new Vector3(0.5f, 0.5f, 0.5f); // increases grass size
    }
}
