﻿using UnityEngine;

public class PressurePlate : MonoBehaviour
{
    private Transform tf; // sets the transform
    private GameObject bridge1; // sets an instance of bridge
    private GameObject bridge2; // sets an instance of bridge
    private GameObject bridge3; // sets an instance of bridge

    public GameObject bridge; // sets the bridge
    public GameObject blueButton; // sets the blue button
    public GameObject RedButton; // sets the red button

    AudioSource click; // sets a audio source

    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>(); // gets the transform
        click = GetComponent<AudioSource>(); // gets the audio source
    }

    void OnTriggerEnter(Collider other)
    {
        click.Play(); // plays the clip
    }

    void OnTriggerStay(Collider other)
    {
        if (bridge1 == null)
        {
            bridge1 = Instantiate(bridge, new Vector3(tf.position.x, tf.position.y, tf.position.z + 1), Quaternion.identity); // Instantiate a bridge
            bridge2 = Instantiate(bridge, new Vector3(tf.position.x, tf.position.y, tf.position.z + 2), Quaternion.identity); // Instantiate a bridge
            bridge3 = Instantiate(bridge, new Vector3(tf.position.x, tf.position.y, tf.position.z + 3), Quaternion.identity); // Instantiate a bridge
            blueButton.SetActive(false); // hides the blue
            RedButton.SetActive(true); // shows the red
        }
    }

    void OnTriggerExit(Collider other)
    {
        Destroy(bridge1, 0.65f); // destroys the bridge
        Destroy(bridge2, 1.25f); // destroys the bridge
        Destroy(bridge3, 1.85f); // destroys the bridge
        blueButton.SetActive(true); // shows the blue
        RedButton.SetActive(false); // hides the red
    }
}
