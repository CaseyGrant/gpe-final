﻿using UnityEngine.UI;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public Text score; // holds the score

    void Start()
    {
        score = GetComponent<Text>(); // gets the text component
    }

    void Update()
    {
        score.text = GameManager.instance.Score.ToString(); // updates the score
    }
}
