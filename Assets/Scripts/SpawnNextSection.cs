﻿using UnityEngine;
using System.Collections;

public class SpawnNextSection : MonoBehaviour
{
    public Transform[] nextSection; // creates an array to fill with pieces
    private bool placed = false; // sets a bool to make sure it only triggers once

    void OnTriggerEnter(Collider other)
    {
        int randomSection = Random.Range(0, nextSection.Length); // gets a random piece

        if (other.tag == "Player" && placed == false) // if the player hits it and placed is not true
        {
            Vector3 spawnPosition = transform.position + new Vector3(0, 0, 8); // sets where the section is spawned
            Quaternion spawnRotation = Quaternion.identity; // makes the rotation 0
            placed = true; // updates placed to be true
            Instantiate(nextSection[randomSection], spawnPosition, spawnRotation); // creates the section
            GameManager.instance.Score++; // updates the score
        }
    }
}